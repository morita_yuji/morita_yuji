package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(",is_deleted ");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); //is_deleted
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setInt(2, user.getBranchId());
            ps.setInt(3, user.getPositionId());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getName());
            ps.setInt(6, user.getIsDeleted());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ログイン機能追記
    public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? ";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);
            //System.out.print(ps.toString());

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //usersテーブルの値を全て取得
    public List<User> getUsers(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int isDeleted = rs.getInt("is_deleted");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setPassword(password);
                user.setName(name);
                user.setIsDeleted(isDeleted);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void isDeleted(Connection connection,User user){
    	PreparedStatement ps = null;
    	try {

    		String sql = "UPDATE users SET is_deleted = ? WHERE id = ?";
    		ps = connection.prepareStatement(sql);
    		ps.setInt(1,user.getIsDeleted());
    		ps.setInt(2,user.getId());

    		ps.executeUpdate();

    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", password = ?");
            sql.append(", name = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setInt(2, user.getBranchId());
            ps.setInt(3, user.getPositionId());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getName());
            ps.setInt(6, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

}