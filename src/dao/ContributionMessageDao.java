package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import beans.ContributionMessage;
import exception.SQLRuntimeException;

public class ContributionMessageDao {

	Date date = new Date();
	Calendar calendar = Calendar.getInstance();
	Calendar cl = Calendar.getInstance();


    public List<ContributionMessage> getUserMessages(Connection connection, int num,String startDate,String endDate,String categorySearch) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("contributions.id as id, ");
            sql.append("contributions.user_id as user_id, ");
            sql.append("contributions.subject as subject, ");
            sql.append("contributions.category as category, ");
            sql.append("contributions.text as text, ");
            sql.append("users.name as name, ");
            sql.append("contributions.created_date as created_date ");
            sql.append("FROM contributions ");
            sql.append("INNER JOIN users ");
            sql.append("ON contributions.user_id = users.id ");
            sql.append("WHERE contributions.created_date ");
            sql.append("BETWEEN ");
            sql.append("? ");
            sql.append("and ");
            sql.append("? ");
            sql.append("and ");
            sql.append("category like ");
            sql.append("? ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1,startDate + " 00:00:00");
            ps.setString(2,endDate + " 23:59:59");
            ps.setString(3,"%" + categorySearch + "%");

            ResultSet rs = ps.executeQuery();

            List<ContributionMessage> ret = toContributionMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<ContributionMessage> toContributionMessageList(ResultSet rs)
            throws SQLException {

        List<ContributionMessage> ret = new ArrayList<ContributionMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String subject = rs.getString("subject");
                String category = rs.getString("category");
                String text = rs.getString("text");
                String name = rs.getString("name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                ContributionMessage message = new ContributionMessage();
                message.setId(id);
                message.setUserId(userId);
                message.setSubject(subject);
                message.setCategory(category);
                message.setName(name);
                message.setText(text);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}