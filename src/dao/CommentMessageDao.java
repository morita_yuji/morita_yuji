package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.CommentMessage;
import exception.SQLRuntimeException;

public class CommentMessageDao {

    public List<CommentMessage> getCommentMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.contribution_id as contribution_id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.text as text, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<CommentMessage> ret = toCommentMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<CommentMessage> toCommentMessageList(ResultSet rs)
            throws SQLException {

        List<CommentMessage> ret = new ArrayList<CommentMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	int contributionId = rs.getInt("contribution_id");
                int userId = rs.getInt("user_id");
                String text = rs.getString("text");
                String name = rs.getString("name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                CommentMessage message = new CommentMessage();
                message.setId(id);
                message.setContributionId(contributionId);
                message.setUserId(userId);
                message.setText(text);
                message.setName(name);
                message.setCreatedDate(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}