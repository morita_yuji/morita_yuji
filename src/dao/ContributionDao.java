package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Contribution;
import exception.SQLRuntimeException;

public class ContributionDao {

    public void insert(Connection connection, Contribution contribution) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO contributions ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", category");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // subject
            sql.append(", ?"); // category
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, contribution.getUserId());
            ps.setString(2, contribution.getSubject());
            ps.setString(3, contribution.getCategory());
            ps.setString(4, contribution.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection,Contribution contribution){
    	PreparedStatement ps = null;
    	try {

    		String sql = "DELETE FROM contributions WHERE id = ?";
    		ps = connection.prepareStatement(sql);
    		ps.setInt(1,contribution.getId());

    		ps.executeUpdate();

    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

}