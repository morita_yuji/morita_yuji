package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;


@WebServlet("/edituser")
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		User user = new User();
		List<Branch> branches = new UserService().getBranches();
        List<Position> positions = new UserService().getPositions();

        user.setId(Integer.parseInt(request.getParameter("id")));
        User editUser = new UserService().getUser(user.getId());
        request.setAttribute("editUser", editUser);
        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);


        request.getRequestDispatcher("edituser.jsp").forward(request, response);

	}

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        //セッションオブジェクトの取得
        HttpSession session = request.getSession();

        User editUser = getEditUser(request);
        List<Branch> branches = new UserService().getBranches();
        List<Position> positions = new UserService().getPositions();


        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.setAttribute("branches", branches);
                request.setAttribute("positions", positions);
                request.getRequestDispatcher("edituser.jsp").forward(request, response);
                return;
            }

            response.sendRedirect("./usermanagement");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);
            request.getRequestDispatcher("edituser.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLoginId(request.getParameter("login_id"));
        editUser.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setPositionId(Integer.parseInt(request.getParameter("position_id")));
        editUser.setPassword(request.getParameter("password"));
        editUser.setName(request.getParameter("name"));
        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String loginId = request.getParameter("login_id");
        String branchId = request.getParameter("branch_id");
        String positionId = request.getParameter("position_id");
        String password = request.getParameter("password");
        String passwords = request.getParameter("passwords");
        String name = request.getParameter("name");

        if (StringUtils.isEmpty(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(branchId) == true) {
            messages.add("支店名を入力してください");
        }
        if (StringUtils.isEmpty(positionId) == true) {
            messages.add("役職名を入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (!password.equals(passwords)) {
        	messages.add("確認用が同じでありません");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}