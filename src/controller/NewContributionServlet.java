package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import beans.User;
import service.ContributionService;

@WebServlet(urlPatterns = { "/newcontribution" })
public class NewContributionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

   @Override
   protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {
	   request.getRequestDispatcher("newcontribution.jsp").forward(request, response);
   }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> errormessages = new ArrayList<String>();

      //バリデーション
        Contribution contributions = getContribution(request);

        if (isValid(request, errormessages) == true) {

        	User user = (User) session.getAttribute("loginUser");

        	Contribution contribution = new Contribution();
        	contribution.setSubject(request.getParameter("subject"));
        	contribution.setCategory(request.getParameter("category"));
        	contribution.setText(request.getParameter("contribution"));

        	contribution.setUserId(user.getId());

        	new ContributionService().register(contribution);

        	response.sendRedirect("./");
       } else {
            session.setAttribute("errorMessages", errormessages);
            request.setAttribute("contributions", contributions);
            request.getRequestDispatcher("newcontribution.jsp").forward(request, response);
            }
    }


    private boolean isValid(HttpServletRequest request, List<String> errormessages) {

        String subject = request.getParameter("subject");
        String category = request.getParameter("category");
        String contribution = request.getParameter("contribution");

        if (StringUtils.isEmpty(subject) == true) {
            errormessages.add("件名を入力してください");
        }
        if (30 < subject.length()) {
            errormessages.add("件名は30文字以下で入力してください");
        }
        if(StringUtils.isEmpty(category) == true) {
        	errormessages.add("カテゴリを入力してください");
        }
        if (10 < category.length()) {
            errormessages.add("カテゴリは10文字以下で入力してください");
        }
        if(StringUtils.isEmpty(contribution) == true) {
        	errormessages.add("投稿内容を入力してください");
        }
        if (1000 < contribution.length()) {
            errormessages.add("投稿内容は10000文字以下で入力してください");
        }

        if (errormessages.size() == 0) {
            return true;
        } else {
            return false;
        }

    }

    //バリデーション
    private Contribution getContribution(HttpServletRequest request)
            throws IOException, ServletException {

        Contribution contribution = new Contribution();
        contribution.setSubject(request.getParameter("subject"));
        contribution.setCategory(request.getParameter("category"));
        contribution.setText(request.getParameter("text"));

        return contribution;
    }
}