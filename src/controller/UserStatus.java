package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class UserStatus
 */
@WebServlet("/userStatus")
public class UserStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

        User user = new User();

        user.setIsDeleted(Integer.parseInt(request.getParameter("status")));
        user.setId(Integer.parseInt(request.getParameter("user_id")));
        //user.setId(user.getId());

        new UserService().isDeleted(user);

        response.sendRedirect("./");
	}

}
