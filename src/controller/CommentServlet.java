package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

   @Override
   protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {
	   request.getRequestDispatcher("newcontribution.jsp").forward(request, response);
   }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    		List<String> errormessages = new ArrayList<String>();
    		HttpSession session = request.getSession();


    		if (isValid(request, errormessages) == true) {
    			User user = (User) session.getAttribute("loginUser");

    			Comment comment = new Comment();
    			comment.setText(request.getParameter("comment"));
    			comment.setContributionId(Integer.parseInt(request.getParameter("message_id")));
    			comment.setUserId(user.getId());

    			new CommentService().register(comment);

    			response.sendRedirect("./");
    		} else {
    			session.setAttribute("errorMessages", errormessages);
                //request.setAttribute("comment", comment);
    			response.sendRedirect("./");
    		}

    }

    private boolean isValid(HttpServletRequest request, List<String> errormessages) {

        String comment = request.getParameter("comment");

        if (StringUtils.isEmpty(comment) == true) {
            errormessages.add("コメントを入力してください");
        } else if (500 < comment.length()) {
        	errormessages.add("コメントは500文字以内で入力してください");
        }
        if (errormessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}