package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Contribution;
import service.ContributionService;

@WebServlet("/deleteContribution")
public class DeleteContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}


	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

        Contribution contribution = new Contribution();
        contribution.setId(Integer.parseInt(request.getParameter("message_id")));

        new ContributionService().deleteContribution(contribution);

        response.sendRedirect("./");
	}

}
