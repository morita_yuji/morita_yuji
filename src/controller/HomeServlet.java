package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CommentMessage;
import beans.ContributionMessage;
import beans.User;
import service.CommentService;
import service.ContributionService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	/*User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }*/

        request.setCharacterEncoding("UTF-8");
        //絞り込み検索
        String startDate = request.getParameter("startdate");
        request.setAttribute("startDate", startDate);
        String endDate = request.getParameter("enddate");
        request.setAttribute("endDate", endDate);

        String categorySearch = request.getParameter("categorysearch");
        request.setAttribute("categorySearch", categorySearch);

        List<ContributionMessage> messages = new ContributionService().getMessage(startDate,endDate,categorySearch);
        List<CommentMessage> comment = new CommentService().getComment();
        //List<User> user = new UserService().getUsers();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");


        request.setAttribute("messages", messages);
        //request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("comments", comment);
        request.setAttribute("users", user);

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}


}