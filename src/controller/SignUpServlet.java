package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<Branch> branches = new UserService().getBranches();
        List<Position> positions = new UserService().getPositions();
    	request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);


        request.getRequestDispatcher("signup.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        List<Branch> branches = new UserService().getBranches();
        List<Position> positions = new UserService().getPositions();
    	request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);

        HttpSession session = request.getSession();

        //バリデーション
        User signUp = getSignup(request);

        if (isValid(request, messages) == true) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLoginId(request.getParameter("login_id"));
            user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
            user.setPositionId(Integer.parseInt(request.getParameter("position_id")));
            user.setPassword(request.getParameter("password"));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("signUp", signUp);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
        String loginId = request.getParameter("login_id");
        String password = request.getParameter("password");
        String passwords = request.getParameter("passwords");


        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        } else if (name.length() > 10 == true) {
        	messages.add("名前は10文字以下で入力してください");
        }

        if (StringUtils.isEmpty(loginId) == true) {
            messages.add("ログインIDを入力してください");
        } else if (loginId.length() < 6 || loginId.length() > 20 == true) {
        	messages.add("ログインIDは6文字以上、20文字以内で入力してください");
        } else if (!loginId.matches("^[0-9a-zA-Z]+$")) {
        	messages.add("ログインIDは半角英数字で入力してください");
        }

        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        } else if (password.length() < 6 || password.length() > 20 == true) {
        	messages.add("パスワードは6文字以上、20文字以内で入力してください");
        } else if (!password.matches("^[0-9a-zA-Z]+$")) {
        	messages.add("パスワードはIDは半角英数字で入力してください");
        } else if (StringUtils.isEmpty(passwords) == true) {
            messages.add("パスワード（確認用）を入力してください");
        }
        if (!password.equals(passwords)) {
        	messages.add("確認用が同じではありません");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

  //バリデーション
    private User getSignup(HttpServletRequest request)
            throws IOException, ServletException {

        User signUp = new User();
        signUp.setName(request.getParameter("name"));
        signUp.setLoginId(request.getParameter("login_id"));

        return signUp;
    }

}