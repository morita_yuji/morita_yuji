package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import beans.ContributionMessage;
import dao.ContributionDao;
import dao.ContributionMessageDao;
public class ContributionService {

    public void register(Contribution contribution) {

        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contributionDao = new ContributionDao();
            contributionDao.insert(connection, contribution);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<ContributionMessage> getMessage(String startDate,String endDate,String categorySearch) {

        Connection connection = null;
        try {
            connection = getConnection();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cl = Calendar.getInstance();

            if(StringUtils.isEmpty(startDate) == true){
            	startDate = "2018-01-01";
            }
            if(StringUtils.isEmpty(endDate) == true){
            	endDate = sdf.format(cl.getTime());
            }

            if(categorySearch == null){
            	categorySearch = "";
            }

            ContributionMessageDao messageDao = new ContributionMessageDao();
            List<ContributionMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM,startDate,endDate,categorySearch);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void deleteContribution(Contribution contribution) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		ContributionDao contributionDao = new ContributionDao();
    		contributionDao.delete(connection,contribution);

    		commit(connection);

    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }


}