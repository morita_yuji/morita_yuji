package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.CommentMessage;
import dao.CommentDao;
import dao.CommentMessageDao;
public class CommentService {

    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<CommentMessage> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentMessageDao commentMessageDao = new CommentMessageDao();
            List<CommentMessage> ret = commentMessageDao.getCommentMessages(connection,LIMIT_NUM);
            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void deleteComment(Comment comment) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		CommentDao commentDao = new CommentDao();
    		commentDao.delete(connection,comment);

    		commit(connection);

    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}
